﻿using AdvanceCommander;
using Commander;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            VariableLong();
        }


        public static Command BuildCommand()
        {
            Command cmd = new Command();
            cmd.set("v", "verbose", "verbose flag", false);
            cmd.set("f", "file", "load file", true);
            cmd.set("s", "stringload", "load string", true);
            cmd.set("d", "dev", "load string", false);

            var args = new string[] { "-v", "-f", "C:\\file\\filepath\\people.txt", "-s", "this is a string","--dev" };

            cmd.parse(args);


            return cmd;
        }

        public static void VariableLong()
        {
            dynamic cmd = BuildCommand();
            Console.WriteLine("this is a string:{0}", cmd.args[4]);
            Console.WriteLine("this is a string:{0}", cmd.stringload);
        }

        public static dynamic AdvanceCommanderBuild()
        {

            string[] args = new string[] { "-p", "28", "-f", "C:\\my file.exe" };
            AdvanceCommand cmd = new AdvanceCommand(args);

            cmd.option("-p,--peppers, <n>", "Add Peppers")
                .option("-f,--file, <value>", "Load file");


            return cmd;
        }


        public static void AdvanceCommanderMembers()
        {
            dynamic cmd = AdvanceCommanderBuild();

            Console.WriteLine(cmd.peppers);
        }
    }
}
