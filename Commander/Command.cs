﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Commander
{

    public class CommandException : Exception
    {
    }

    public class Command : DynamicObject
    {

        public class Set
        {
            public string command;
            public string longcommand;
            public string help;
            public bool hasValue;
            public bool has(string s)
            {
                return (command == s || longcommand == s);
            }
        }

        public IList<Set> CommandSet;



        public Command()
        {
            CommandSet = new List<Set>();
            Version = "1.0.0";
        }

        public Command(string version)
            : this()
        {
            Version = version;
        }

        public string Version { get; set; }

        public string help()
        {
            var sb = new StringBuilder();

            sb.Append(string.Format("Application Version:{0}{1}", this.Version, Environment.NewLine));
            foreach (Set s in CommandSet)
            {

                string format;
                if (s.help != null)
                {
                    format = string.Format("    -{0}, --{1}, {2}{3}", s.command, s.longcommand, s.help, Environment.NewLine);
                }
                else if (s.longcommand != null)
                {
                    format = string.Format("    -{0}, --{1}{2}", s.command, s.longcommand, Environment.NewLine);
                }
                else
                {
                    format = string.Format("    -{0}{1}", s.command, Environment.NewLine);
                }
                sb.Append(format);
            }
            return sb.ToString();
        }

        public void printHelp()
        {
            Console.WriteLine(help());
        }


        public Command set(string command, string longcommand, string help, bool hasValue)
        {
            var set = new Set()
            {
                command = command,
                longcommand = longcommand,
                help = help,
                hasValue = hasValue
            };

            CommandSet.Add(set);
            return this;
        }

        public Command set(string command, string longcommand, bool hasValue)
        {

            set(command, longcommand, null, hasValue);
            return this;
        }

        public Command set(string command, bool hasValue)
        {
            set(command, (String)null, hasValue);
            return this;
        }

        public Command Parse(String[] args)
        {
            this.args = args;
            return this;
        }

        public Command parse(String[] args)
        {
            return Parse(args);
        }

        public string[] args;

        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            var name = binder.Name;

            return TryGetMember(name, out result);
        }

        public bool TryGetMember(string name, out object result)
        {
            object match;
            IList<Set> sets = new List<Set>();
            foreach (Set s in CommandSet)
            {
                if (s.has(name))
                {
                    sets.Add(s);
                }
            }
            FindMatch(out match, sets, args);
            result = match;
            return true;
        }

        public object this[string key]
        {
            get
            {
                object result;
                TryGetMember(key, out result);
                return result;
            }
        }

        public static bool FindMatch(out object match, IList<Set> sets, string[] args)
        {
            object match1;
            object match2;

            FindCmd(out match1, sets, args);
            FindCmdLong(out match2, sets, args);

            if (match1 != null && match1.GetType() == typeof(bool))
            {
                match = (bool)match1 || (bool)match2;
            }
            else
            {
                match = match1 ?? match2;
            }
            return true;
        }

        public static void FindCmd(out object match, IList<Set> commandset, string[] args)
        {
            match = null;

            foreach (Set c in commandset)
            {
                for (int i = 0; i < args.Length;i++)
                {
                    var arg = args[i];
                    if (arg == string.Format("-{0}", c.command))
                    {
                        if (c.hasValue && i < args.Length - 1)
                        {
                            match = args[i + 1];
                        }else if(c.hasValue){
                            match = null;
                        }
                        else {
                            match = true;
                        }
                        break;
                    }
                }

            }
        }

        public static void FindCmdLong(out object match, IList<Set> commandset, string[] args)
        {
            match = null;
            foreach (Set c in commandset)
            {
                for (int i = 0; i < args.Length;i++)
                {
                    var arg = args[i];
                    if (arg == string.Format("--{0}", c.command))
                    {
                        if (c.hasValue && i < args.Length - 1)
                        {
                            match = args[i + 1];
                        }else if(c.hasValue){
                            match = null;
                        }
                        else {
                            match = true;
                        }
                        break;
                    }
                }
            }
        }

    }
}
