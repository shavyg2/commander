﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Commander
{
    class Program
    {
        static void Main(string[] args)
        {

            args = new string[] { "-verbose -d" };

            dynamic command = new Command("1.0.0");
            command.parse(args);
            command.set("v","verbose", false);
            command.set("r", "repeat", false);
            command.set("d", "dry-run", false);
            command.printHelp();

            if (command.v){
                Console.WriteLine("Success v");
            }
            else {
                Console.WriteLine("Failure v");
            }

            if (command.verbose)
            {
                Console.WriteLine("Success verbose");
            }
            else
            {
                Console.WriteLine("Failure verbose");
            }


            if (command.r)
            {
                Console.WriteLine("Success r");
            }
            else
            {
                Console.WriteLine("Failure r");
            }


            if (command["dry-run"]!=null && command["dry-run"])
            {
                Console.WriteLine("Success d");
            }
            else
            {
                Console.WriteLine("Failure d");
            }
            Console.ReadLine();
        }
    }
}
