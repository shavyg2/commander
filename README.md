#Commander

Commander gives you the ability to parse commands and get values out of them.
The reason for commander is to make command line applications easier to get started.


----------

	Commander cmd = new Commander("1.0.0");
This is enough to initial Commander. The String in the middle will be displayed as the version of the commander that is being used.

---------------

	Commander cmd = new Commander();
	cmd.set("n","name","This will set the name of the value of a command",true);

This will all the developer to use the n or name flag at the command line.


####Example
	Application.exe -n charlie
	Application.exe --name charlie
	Application.exe /n charlie
 
The boolean variable at the end is to allow a user to take the next non space seperated set of string as an parameter.
Inside the program you will have access to that parameter.

	cmd.name //charlie
	cmd.n //charlie


_**Shavauhn Gabay**_