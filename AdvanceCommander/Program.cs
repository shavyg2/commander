﻿#define test
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdvanceCommander
{
    public class AdvanceCommand : DynamicObject
    {

        private string[] args;
        private string _version;
        public string Version { get { return _version; } }

        IList<Flag> Flags;


        public class Flag
        {
            private string parameter;

            private string description;

            public string Parameter
            {
                get
                {
                    return parameter;
                }

                set
                {
                    parameter = value;
                }
            }

            public string Single
            {
                get
                {
                    string val = null;
                    int pos = Parameter.IndexOf('-');
                    if (Parameter[pos + 1] != '-')
                    {
                        val = Parameter[pos + 1].ToString();
                    }

                    return val;
                }
            }

            public string Full
            {
                get
                {
                    string val = null;
                    int pos = Parameter.IndexOf("--");
                    int space_end, comma_end;
                    int end = int.MaxValue;
                    bool use_end = false;
                    if ((space_end = Parameter.IndexOf(' ', pos)) > -1)
                    {
                        use_end = true;
                    }

                    if ((comma_end = Parameter.IndexOf(',', pos)) > -1)
                    {
                        use_end = true;
                    }

                    if (use_end)
                    {
                        space_end = space_end > -1 ? space_end : Int32.MaxValue;
                        comma_end = comma_end > -1 ? comma_end : Int32.MaxValue;
                        end = Math.Min(comma_end, space_end);
                        val = Parameter.Substring(pos + 2,(end)-(pos+2));
                    }
                    else
                    {
                        val = Parameter.Substring(pos + 2);
                    }

#if test
                    Console.WriteLine("use_end:{0},end:{1}\nVal:{2}\nComma end:{3}",use_end,end,val,comma_end);
#endif

                    return val;
                }
            }

            public string Regex
            {
                get
                {
                    string val = null;
                    if (Single != null && Full != null)
                    {
                        val = string.Format("(?:-{0}|--{1})", Single, Full);
                    }
                    else if (Single != null)
                    {
                        val = string.Format("(?:-{0})", Single);
                    }
                    else if (Full != null)
                    {
                        val = string.Format("(?:--{0})", Full);
                    }

                    if (hasInt)
                    {
                        val += " ([0-9]+)";
                    }
                    else if (hasString)
                    {
                        val += " ([A-z0-9_]+)";
                    }
                    else if (hasList) {
                        val += " ([A-z0-9_,]+)";
                    }


                    return val;
                }
            }

            public string Description
            {
                get
                {
                    return description;
                }

                set
                {
                    description = value;
                }
            }

            public bool has(string term)
            {
                bool val = false;
                foreach (string s in parameter.Split(','))
                {
                    var str = s.Replace("-", string.Empty);
                    val = val || str == term;
                }
                return val;
            }

            public bool hasInt
            {
                get
                {
                    return Parameter.IndexOf("<n>") > -1;
                }
            }

            public bool hasString
            {
                get
                {
                    return Parameter.IndexOf("<value>") > -1;
                }
            }

            public bool hasList {
                get {
                    return Parameter.IndexOf("<list>") > -1;
                }
            }
        }


        public AdvanceCommand()
        {
            Flags = new List<Flag>();
        }


        public AdvanceCommand(string[] args)
            : this()
        {
            parse(args);
        }

        public AdvanceCommand Parse(string[] args)
        {
            return Parse(args);
        }

        public AdvanceCommand parse(string[] args)
        {
            this.args = args;
            return this;
        }

        public AdvanceCommand option(string parameter)
        {

            return this;
        }

        public AdvanceCommand option(string parameter, string description)
        {
            Flags.Add(new Flag() { Parameter = parameter, Description = description });
            return this;
        }

        public bool has(string term)
        {
            foreach (Flag f in Flags)
            {
                if (f.has(term))
                {
                    return true;
                }
            }
            return false;
        }

        public bool has(string term, out Flag flag)
        {
            foreach (Flag f in Flags)
            {
                if (f.has(term))
                {
                    flag = f;
                    return true;
                }
            }
            flag = null;
            return false;
        }

        public bool hasFlag(ref IList<Flag> flaglist, string term)
        {
            bool val = false;
            foreach (Flag flag in Flags)
            {
                if (flag.has(term))
                {
                    flaglist.Add(flag);
                    if (!val)
                        val = true;
                }
            }
            return val;
        }

        public string raw
        {
            get
            {
                string str;
                StringBuilder sb = new StringBuilder();

                foreach (string _s in args)
                {
                    string s = _s;

                    if (s.IndexOf(' ') > -1)
                    {
                        s = string.Format("\"{0}\"", s);
                    }

                    sb.Append(' ');
                    sb.Append(s);
                }

                str = sb.ToString().Trim();
                return str;
            }
        }


        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            string name = binder.Name;
            result = null;
            Flag flag;
            if (has(name, out flag))
            {
                var regexString = BuildRegex(flag);
                var regex = new Regex(regexString);
                var match = regex.Match(raw);
                if (match.Success)
                {
                    result = match.Groups[1].Value;
                    if (flag.hasInt) {
                        result = Int32.Parse((string)result);
                    }
                }
            }

            return true;
        }

        

        public string BuildRegex(Flag flag)
        {
            string regex = flag.Regex;
            return regex;
        }



    }
}
