﻿using AdvanceCommander;
using Commander;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommanderTest
{
    [TestFixture]
    public class Test
    {

        public static Command BuildCommand() {
            Command cmd = new Command();
            cmd.set("v", "verbose", "verbose flag",false);
            cmd.set("f", "file", "load file", true);
            cmd.set("s", "stringload", "load string", true);
            cmd.set("a", "anotherstringload", "load string", true);
            cmd.set("d", "dev", "load string", false);

            var args = new string[] { "-v", "-f", "C:\\file\\filepath\\people.txt", "-s", "this is -a string", "-a", "this is a string" ,"--dev"};

            cmd.parse(args);
            

            return cmd;
        }

        [Test]
        public static void SingleFlag() {
            dynamic cmd = BuildCommand();
            Assert.IsTrue(cmd.v);
            Assert.IsTrue(cmd.verbose);
        }


        [Test]
        public static void Variable()
        {
            dynamic cmd = BuildCommand();
            Assert.AreEqual("this is -a string", cmd.s);
        }


        [Test]
        public static void VariableLong()
        {
            dynamic cmd = BuildCommand();
            Assert.AreEqual("this is -a string", cmd.args[4]);
            Assert.AreEqual("this is -a string", cmd.stringload);
            Assert.AreEqual("this is a string", cmd.anotherstringload);
        }

        public static void SpacingTest()
        {

        }

        public static AdvanceCommand.Flag GetFlag()
        {
            var flag = new AdvanceCommand.Flag();
            flag.Parameter = "-p,--peppers, <n>";
            flag.Description = "add peppers";
            return flag;
        }
        public static AdvanceCommand.Flag GetFlag2()
        {
            var flag = new AdvanceCommand.Flag();
            flag.Parameter = "-p,--peppers, <value>";
            flag.Description = "add peppers";
            return flag;
        }

        [Test]
        public static void FlagTest()
        {
            var flag = GetFlag();
            Assert.IsTrue(flag.has("p"));

            Assert.IsTrue(flag.has("peppers"));

            Assert.AreEqual("p", flag.Single);

            Assert.AreEqual("peppers", flag.Full);

            Assert.AreEqual("(?:-p|--peppers) ([0-9]+)", flag.Regex);

        }
    }

    [TestFixture]
    public class TestAdvance {
        public static dynamic AdvanceCommanderBuild()
        {

            string[] args = new string[] { "-p","28","-f", "C:\\my file.exe" };
            AdvanceCommand cmd = new AdvanceCommand(args);

            cmd.option("-p,--peppers, <n>", "Add Peppers")
                .option("-f,--file, <value>","Load file");


            return cmd;
        }

        [Test]
        public static void AdvanceCommanderHas()
        {
            dynamic cmd = AdvanceCommanderBuild();

            Assert.IsTrue((bool)cmd.has("peppers"));
            Assert.IsTrue((bool)cmd.has("p"));
            Assert.IsTrue((bool)cmd.has("f"));
            Assert.IsTrue((bool)cmd.has("file"));
        }


        [Test]
        public static void AdvanceCommanderRaw()
        {
            AdvanceCommand cmd = AdvanceCommanderBuild();

            Assert.AreEqual("-p 28 -f \"C:\\my file.exe\"", cmd.raw);
        }


        [Test]
        public static void AdvanceCommanderRegex()
        {
            AdvanceCommand cmd = AdvanceCommanderBuild();
            AdvanceCommand.Flag f;
            cmd.has("p", out f);
            var regex=cmd.BuildRegex(f);

            Assert.AreEqual("(?:-p|--peppers) ([0-9]+)", regex);
        }


        [Test]
        public static void AdvanceCommanderMembers()
        {
            dynamic cmd = AdvanceCommanderBuild();

            Assert.AreEqual(28, cmd.peppers);
            Assert.AreEqual(28, cmd.p);
        }

       

    }
}
